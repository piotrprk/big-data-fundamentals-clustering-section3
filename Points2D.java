import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Scanner;

class Point{
	double x;
	double y;
	int parent; // parent index to make clusters
	
	public Point() {
		this.x = 0d;
		this.y = 0d;
	}
	public Point(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public String toString() {
		return "("+ String.format("%.2f",x) + ", " + String.format("%.2f",y) +")";
	}	 
    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Point)) {
            return false;
        }
        Point e = (Point) o;
        return  x == e.x && y == e.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }	
}
class PointP extends Point{
	int parent; // parent index to make clusters
	
	public PointP () {
		super();
		this.parent = -1;
	}
	public String toString() {
		return "("+x + ", " + y +")" + ((this.parent > -1 )? " [p:" + this.parent + "]": "" );
	}	 
	public PointP (double x, double y) {
		this.x = x;
		this.y = y;
		this.parent = -1;
	}
	public PointP (Point point, int parent) {
		super(point.x, point.y);
		this.parent = parent;
	}	
	

}
class Centroid{
	Point center;
	ArrayList<Point> points;
	
	public Centroid() {
		this.center = new Point();
		this.points = new ArrayList<>();
	}
	public Centroid(Point p) {
		this.points = new ArrayList<>();
		this.points.add(p);
		this.center = new Point(p.x, p.y);
	}
	public void add(Point p) {
		this.points.add(p);
	//	this.setCenter();
	}
	public void setCenter() {
		// set center as average
		double sum_x = 0d;
		double sum_y = 0d;
		int i = 0;
		for (Point p:points) {
			sum_x += p.x;
			sum_y += p.y;
			i++;
		}
		this.center.x = sum_x / i;
		this.center.y = sum_y / i;
	}	
	public String toString() {
		return center + " " + points;
	}
}
public class Points2D {
	ArrayList<Point> points;
	int n; // number of points
	int K; // number of clusters
	
	public Points2D() {
		this.points = new ArrayList<>();
	}
	public Points2D(String file_name) {
		this.points = new ArrayList<>();
		this.importTXT(file_name, -1);
		this.loadingCheck();
	}	
	public void importTXT(String file_name, int n_lines) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			int k = 0; // count already red lines
			while(scanner.hasNextLine() && (k < n_lines || n_lines == -1)) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\t");	// get data by split with TAB
					// add data
					if (k > 0) {
						double x = Double.valueOf(line[0]);
						double y = Double.valueOf(line[1]);
						this.points.add(new Point(x, y));							
					}
					// extract check values
					else {
						this.K = Integer.valueOf(line[0]);
						this.n = Integer.valueOf(line[1]);
					}
					//					
				}	
				k++;
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}				
	}
	// check points loading for errors
	public void loadingCheck() {
		System.out.println();
		System.out.println("check points: " + 
				this.n + " " +
				((this.n != this.points.size()) ? " ERROR ": " OK "));
		System.out.println("clusters: " + this.K);
		System.out.println();
	}
	public void printAll() {
		for (Point p:this.points) {
			System.out.println(p);
		}
	}
	private Point maxValuePoint(ArrayList<Point> points) {
		double max_x = 0d;
		double max_y = 0d;
		for (Point p:points) {
			if (p.x > max_x)
				max_x = p.x;
			if (p.y > max_y)
				max_y = p.y;
		}
		return new Point(max_x, max_y);		
	}
	private Point minValuePoint(ArrayList<Point> points) {
		double min_x = Double.MAX_VALUE;
		double min_y = Double.MAX_VALUE;
		for (Point p:points) {
			if (p.x < min_x)
				min_x = p.x;
			if (p.y < min_y)
				min_y = p.y;
		}
		return new Point(min_x, min_y);		
	}
	
	private ArrayList<Centroid> findStartinPoints(int K, ArrayList<Point> points){
		// find max value point		
		
		ArrayList<Centroid> P = new ArrayList<>(K);		
		Point p_min = minValuePoint(points);
		Point p_max = maxValuePoint(points);
		P.add(new Centroid(p_min));
		double dist_x = p_max.x - p_min.x;
		double dist_y = p_max.y - p_min.y;
		for (int i=1; i<K-1; i++) {
			double x = i * dist_x/K;
			double y = i * dist_y/K;
			Point p = new Point(x, y);
			P.add(new Centroid(p));
		}
		P.add(new Centroid(p_max));
		int i = 0;
		for (Centroid p:P) {
			System.out.println("point " + ++i +" : "+ p.center);	
		}		
		
		return P;
	}
	// K-means clustering
	public void Kmeans(){
		// sample input data to find starting points quicker		
		ArrayList<Point> sub_list = new ArrayList<>(this.points.subList(0, this.points.size()/3));
		// find starting points K-centroids
		ArrayList<Centroid> starting_points = hierarchicalClustering(this.K, sub_list);

		//ArrayList<Centroid> starting_points = findStartinPoints(this.K, this.points);
		

		// declare clusters
		ArrayList<Centroid> clusters = new ArrayList<>(this.K);
		// set clusters starting points
		for (Centroid c:starting_points) {
			Centroid a = new Centroid(c.center);
			// add new centroid with starting point cords
			clusters.add(a);
		}
		int iteration = 0;
		// loop ends when no point changes its cluster
		boolean changed = true;
		while (changed) {
			// remember cluster size
			int[] cluster_size = new int[this.K];
			// clear points array for every cluster
			for (int i=0; i<clusters.size(); i++) {
				cluster_size[i] = clusters.get(i).points.size();
				clusters.get(i).points.clear();
			}
			// assign points to clusters by distance
			for (Point p:this.points) {
				int index = 0;
				int min_index = -1;
				double min = Double.MAX_VALUE;
				for (Centroid c:clusters) {
					double dist = this.distance(p, c.center);	
					if (dist < min) {
						min = dist;
						min_index = index;
					}
					index++;
				}	
				// add point to cluster
				if (min_index > -1) {
					clusters.get(min_index).points.add(p);
				}
			} 
			// find if cluster size changed
			int difference = 0;
			for (int i=0 ;i<this.K; i++) {
				int size = clusters.get(i).points.size();
				if (size != cluster_size[i])
					difference++;						
			}
			if (difference > 0)
				changed = true;
			else 
				changed = false;
			// recalculate centroids with new set of points
			for (Centroid c:clusters) {
				c.setCenter();
			}
			iteration++;
			// print all
			System.out.println();
			System.out.println("iteration: " + iteration);
			for (Centroid c:clusters) {
				//System.out.println(c + " size: " + c.points.size());
				System.out.println(String.format("%.2f",c.center.x) + "\t" + String.format("%.2f",c.center.y) + "\t" + c.points.size());				
			}			
		}
	}
	private double distance (Centroid a, Centroid b) {
		return this.distance(a.center, b.center);
	}
	private double distance (Point a, Point b) {
		return Math.sqrt(Math.pow((b.x - a.x), 2) + Math.pow((b.y - a.y), 2)); 
	}
	// k - number of clusters to find
	// cluster by minimum distance of centroids
	public ArrayList<Centroid> hierarchicalClustering(int k, ArrayList<Point> points) {
		// declare centroids array
		// add all points as centroids
		ArrayList<Centroid> centroids = new ArrayList<>();
		for (Point p:points) {
			centroids.add(new Centroid(p));
		}
		int iter = 0;		
		// iterate until k clusters are found
		while (centroids.size() > k) {
			// print
			System.out.println("iteration: " + iter++);
			for (Centroid c:centroids) {
				System.out.print(String.format("Center: %.2f",c.center.x) + " size: " + c.points.size());
				System.out.print(" Points: [ ");
				for(int i=0; i<c.points.size(); i++) {
					System.out.printf("%.0f",c.points.get(i).x);
					if (i < c.points.size() -1 )
						System.out.print(", ");
				}
				System.out.print(" ] \n");
			}			
			// set distances a-b by point index 
			// find minimum distance between centres
			double min = Double.MAX_VALUE;
			int min_i = -1;
			int min_j = -1;
			for (int i=0; i < centroids.size(); i++) {
				for (int j=0; j < centroids.size(); j++) {
					if (i != j) {
						double dist = this.distance(centroids.get(i), centroids.get(j));
						if (dist < min) {
							min = dist;
							min_i = i;
							min_j = j;
						}
					}
				}
			}		
			// merge two closest points
			Centroid a = centroids.get(min_j);
			centroids.get(min_i).points.addAll(a.points);
			centroids.get(min_i).setCenter();
			// remove merged centroid
			centroids.remove(min_j);			
		}			
		
		return centroids;
	}
	// k - number of clusters to find
	// type 2 clustering - by minimum distance of points
	public ArrayList<Centroid> hierarchicalClusteringT2(int k, ArrayList<Point> points) {
		// declare centroids array
		// add all points as centroids
		ArrayList<Centroid> centroids = new ArrayList<>();
		for (Point p:points) {
			centroids.add(new Centroid(p));
		}
		int iter = 0;
		// iterate until k clusters are found		
		while (centroids.size() > k) {
			// print
			System.out.println("iteration: " + iter++);
			for (Centroid c:centroids) {
				System.out.print("Cluster size: " + c.points.size());
				System.out.print(" Points: [ ");
				for(int i=0; i<c.points.size(); i++) {
					System.out.printf("%.0f",c.points.get(i).x);
					if (i < c.points.size() -1 )
						System.out.print(", ");
				}
				System.out.print(" ] \n");
			}			
			// set distances a-b by point index 
			// find minimum distance between points
			double min = Double.MAX_VALUE;
			int min_i = -1;
			int min_j = -1;
			for (int i=0; i < centroids.size(); i++) {
				for (int j=0; j < centroids.size(); j++) {
					if (i != j) {
						double dist = this.MINdistanceBetweenPoints(centroids.get(i), centroids.get(j));
						if (dist < min) {
							min = dist;
							min_i = i;
							min_j = j;
						}
					}
				}
			}		
			// merge two closest points
			Centroid a = centroids.get(min_j);
			centroids.get(min_i).points.addAll(a.points);
			centroids.get(min_i).setCenter();
			// remove merged centroid
			centroids.remove(min_j);			
		}			
		
		return centroids;
	}	
	private double MINdistanceBetweenPoints(Centroid a, Centroid b) {
		double min = Double.MAX_VALUE;
		for (Point p1:a.points) {
			for (Point p2:b.points) {
				double dist = distance(p1, p2);
				if( dist < min) {
					min = dist; 
				}
			}
		}
		return min;
	}
}
