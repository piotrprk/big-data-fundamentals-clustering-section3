
public class A3 {
	public static void main(String[] args) {
		
		// load file
		//String file_name = "ca-netscience.mis";
		//String file_name = "test-case-3.txt";
		String file_name = "MA1-Part2.txt";
		//
		Points2D data = new Points2D(file_name);
		
		data.printAll();
		//
		//data.Kmeans();
		data.hierarchicalClustering(1, data.points);
		data.hierarchicalClusteringT2(1, data.points);
	}
}
