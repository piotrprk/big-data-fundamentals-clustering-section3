import java.util.ArrayList;

public class GraphVariant extends Graph{
	int[][] adj_matrix; // edges as matrix
	
	public GraphVariant() {		
	}
	private int nodeIndex(String name) {
		int i = 0;
		for (String n:this.nodes.keySet()) {
			if (n.equals(name))
				return i;
			i++;
		}
		return -1;
	}
	public void buildMatrix() {
		this.adj_matrix = new int[this.nodes.size()][this.nodes.size()];
		int i = 0; //row
		//int j = 0; //column
		for(String name:this.nodes.keySet()) {
			for (Edge e:this.nodes.get(name).edges) {
				// j - column no
				int j = this.nodeIndex(e.node_name);				
				adj_matrix[i][j] = e.weight;
			}
			i++;
		}
	}
	public void printMatrix(int[][] m) {
		String[][] m2 = new String[m.length][m.length];
		for (int i=0; i < m.length; i++) {
			for (int j=0; j<m[i].length; j++) {
				m2[i][j] = String.valueOf(m[i][j]);
			}
		}
		this.printMatrix(m2);
	}
	public void printMatrix(double[][] m) {
		String[][] m2 = new String[m.length][m.length];
		for (int i=0; i < m.length; i++) {
			for (int j=0; j<m[i].length; j++) {
				m2[i][j] = String.format("%.02f", m[i][j]);
			}
		}
		this.printMatrix(m2);
	}	
	public void printMatrix(String[][] m) {		
		String format = "%-5s";
		ArrayList<String> node_names = new ArrayList<>(this.nodes.keySet());
		
		System.out.print("\n");
		System.out.printf(format, "");
		for (int i=0; i < m.length; i++) {
			System.out.printf(format, node_names.get(i));
		}
		System.out.print("\n");
		System.out.printf(format, "");		
		for (int i=0; i < m.length; i++) {
			System.out.printf( format, "-");
		}
		System.out.print("\n");

		for (int i=0; i < m.length; i++) {
			for (int j=0; j<m[i].length; j++) {
				if (j == 0)
					System.out.printf( "%-4s|", node_names.get(i));
				System.out.printf(format, m[i][j] + " ");
			}
			System.out.print("\n");
		}
	}
	private double[][] matrixMultiply(double[][] A, double[][] B){
		int n = A.length;
		int p = B[0].length;
		double[][] ret = new double[n][p];
		for (int i = 0; i < n; i++) {
			for (int j=0; j<p; j++) {
				for (int r=0; r<A[i].length; r++) {
					ret[i][j] += A[i][r]*B[r][j];
				}				
			}
		}		
		return ret;
	}
	// MCL algorithm
	// runs - how many times run this 
	// p T matrix multiplier
	// r boosting parameter tij = tij^r / sum(k=1->n)tkj^r
	// tr pruning level threshold 0 - 0.5
	public void MCL(int runs, int r, int p, double tr) {
		int n = adj_matrix.length;
		// copy int matrix to double
		double[][] matrix = new double[n][n];
		for (int i = 0; i < n; i++) {
			for (int j=0; j<n; j++) {
				matrix[i][j] = Double.valueOf(adj_matrix[i][j]);
			}
		}
		// add I matrix
		for (int i=0; i < adj_matrix.length; i++) {
			matrix[i][i] = 1d;
		}
		
		// transition matrix Tg
		// tij = mij / sum(1-n)mkj
		double[][] T = new double[n][n];
		double[] sum = new double[n];
		for (int i=0; i < n; i++) {
			for (int j=0; j<matrix[i].length; j++) {
				sum[j] = sum[j] + matrix[i][j];
			}
		}
		for (int i=0; i < n; i++) {
			for (int j=0; j<matrix[i].length; j++) {
				T[i][j] = (double)matrix[i][j] / (double) sum[j];
			}
		}
		System.out.println("");
		System.out.println(" T matrix ");
		this.printMatrix(T);
		// run this multiple times
		int k = 0;
		while ( k++ < runs ) {
			//multiply T by T p times
				double [][] Tbis = T;
				for (int i=1; i<p; i++) {			
					Tbis = this.matrixMultiply(Tbis, T);
				}
				T = Tbis;
			
			// inflate values by r
			double[] r_sum = new double[n]; 
			for (int i=0; i < n; i++) {
				for (int j=0; j< T[i].length; j++) {
					r_sum[j] = r_sum[j] + Math.pow(T[i][j], r);
				}
			}
			for (int i=0; i < n; i++) {
				for (int j=0; j< T[i].length; j++) {					
					T[i][j] = Math.pow(T[i][j], r) / r_sum[j];
					// prune values that are less than 
					if (T[i][j] <= tr)
						T[i][j] = 0d;
				}
			}
			// print it
			System.out.println();			
			System.out.println(" iteration k: " + k);
			this.printMatrix(T);
			
		}
		
		
		//
	}
}
